IMPORTANT NOTES FOR REVIEWER
============================

1. The project fully implements the requested user story with the acceptance criteria.
2. The project executes fine on both simulators and device. However, while running on the simulator please make sure to enter a dummy location into Simulator. Also, the link to open google maps app wouldn't work in simulator. 
3. iOS 9 onwards supported. Tested on iPhone 5 onwards simulators and on actual iPhone 6s device running iOS 10.3.2. The project supports both orientations. 
4. The project has been developed on latest xcode 8.3.3 with Swift 3.1 without any warnings or errors.
5. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern. 
6. Test driven development approach has been adopted by writting test first with XCTest. Only suitable test cases have been written considering the time limitation and effort involved. Project Test suite has 45 test cases giving 70.50% project test covering. Please see the attached "TestCoverage_snapshot.png"
7. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
8. With more time, following things could be implemented/improved:
	* Aiming to achieve 100% test coverage by writing test cases for map screen and its related MVVM-C module components. Most of those test cases would be same as bar list screen MVVM-C module, whose test cases are already thoroughly written.
	* Showing activity indicator while the records are getting populated.
    * Some helpful console logs along with some user info messages are currently being shown in Xcode console incase of any issue with retriving data, this could be improved by showing alert to the user.
9. This project is built on best software engineering practices. To name a few: SOLID principles, composition over inheritance, program to interface not implementation, loosely coupled architecture and TDD

Thanks for your time.

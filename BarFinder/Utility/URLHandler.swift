//
//  UrlHandler.swift
//  BarFinder
//
//  Created by Waheed Malik on 16/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class URLHandler {
    static func openUrl(ofBar bar: BarProtocol) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?q=\(bar.latitude!),\(bar.longitude!)")!)
        } else {
            print("Can't use comgooglemaps://");
        }
    }
}

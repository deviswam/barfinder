//
//  BarListTableViewDelegate.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class BarListTableViewDelegate: NSObject, UITableViewDelegate {
    var viewModel: BarListViewModelProtocol?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selectedBar(at: indexPath.row)
    }
}

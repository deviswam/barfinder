//
//  BarListTableViewCell.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol BarListTableViewCellProtocol {
    func configCell(with barViewModel: BarViewModelProtocol)
}

class BarListTableViewCell: UITableViewCell, BarListTableViewCellProtocol {
    func configCell(with barViewModel: BarViewModelProtocol) {
        self.textLabel?.text = barViewModel.name
        self.detailTextLabel?.text = barViewModel.distance
    }
}

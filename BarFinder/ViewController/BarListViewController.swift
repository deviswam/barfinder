//
//  BarListViewController.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class BarListViewController: UIViewController {

    @IBOutlet weak var barListTableView: UITableView!
    var barListTableViewDataSource: UITableViewDataSource?
    var barListTableViewDelegate: UITableViewDelegate?
    var viewModel: BarListViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        barListTableView.dataSource = barListTableViewDataSource
        barListTableView.delegate = barListTableViewDelegate
        
        viewModel?.searchNearbyBars()
    }
}

extension BarListViewController: BarListViewModelViewDelegate {
    func barsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.barListTableView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch nearby bar list")
        }
    }
}

//
//  BarsMapViewController.swift
//  BarFinder
//
//  Created by Waheed Malik on 16/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit
import MapKit

class BarsMapViewController: UIViewController {
    
    @IBOutlet weak var barsMapView: MKMapView!
    var barsMapViewDelegate: MKMapViewDelegate?
    var viewModel: BarsMapViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        barsMapView.delegate = barsMapViewDelegate
        
        viewModel?.searchNearbyBars()
    }
    
}

extension BarsMapViewController: BarsMapViewModelViewDelegate {
    func barAnnotationsLoaded(with result: Result<[MKAnnotation]>) {
        switch result {
        case .success(let annotations):
            self.barsMapView.showAnnotations(annotations, animated: true)
        case .failure:
            print("Info (Inform user): Unable to fetch nearby bar list")
        }
    }
}

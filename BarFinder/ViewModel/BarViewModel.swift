//
//  BarViewModel.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation
import MapKit

protocol BarViewModelProtocol {
    var name: String { get }
    var distance: String { get }
}

class BarViewModel: NSObject, BarViewModelProtocol, MKAnnotation {
    var name: String = ""
    var distance: String = ""
    
    var coordinate: CLLocationCoordinate2D
    var title: String? {
        return name
    }
    
    var subtitle: String? {
        return distance
    }
    
    init(bar: BarProtocol) {
        if let barName = bar.name {
            self.name = barName
        }
        
        if let distance = bar.distance {
            self.distance = String(format: "%.2fm", distance) //  "\(distance)m"
        }
        
        if let lat = bar.latitude, let lon = bar.longitude {
            coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        } else {
            coordinate = CLLocationCoordinate2D()
        }
    }
}

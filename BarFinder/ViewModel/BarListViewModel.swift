//
//  BarListViewModel.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol BarListViewModelCoordinatorDelegate: class {
    func didSelect(bar: BarProtocol)
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol BarListViewModelViewDelegate: class {
    func barsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol BarListViewModelProtocol {
    func searchNearbyBars()
    func numberOfBars() -> Int
    func bar(at index: Int) -> BarViewModelProtocol?
    func selectedBar(at index: Int)
}

class BarListViewModel: BarListViewModelProtocol {
    // MARK: PRIVATE VARIABLES
    private let placesManager: PlacesManagerProtocol
    private weak var viewDelegate: BarListViewModelViewDelegate!
    private weak var coordinatorDelegate: BarListViewModelCoordinatorDelegate!
    private var bars: [BarProtocol]?
    
    // MARK: INITIALIZER
    init(placesManager: PlacesManagerProtocol, viewDelegate: BarListViewModelViewDelegate, coordinatorDelegate: BarListViewModelCoordinatorDelegate) {
        self.placesManager = placesManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func searchNearbyBars() {
        self.placesManager.searchNearbyBars { [weak self] (result: Result<[BarProtocol]>) in
            var vmResult: Result<Void>!
            switch result {
            case .success(let bars):
                self?.bars = bars
                vmResult = .success()
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.barsLoaded(with: vmResult)
        }
    }
    
    func numberOfBars() -> Int {
        guard let bars = self.bars else { return 0 }
        return bars.count
    }
    
    func bar(at index: Int) -> BarViewModelProtocol? {
        if let bars = self.bars, index < bars.count {
            return BarViewModel(bar: bars[index])
        }
        return nil
    }
    
    func selectedBar(at index: Int) {
        if let bars = self.bars, index < bars.count {
            let bar = bars[index]
            self.coordinatorDelegate.didSelect(bar: bar)
        }
    }
}

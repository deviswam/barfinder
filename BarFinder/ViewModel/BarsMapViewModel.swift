//
//  BarsMapViewModel.swift
//  BarFinder
//
//  Created by Waheed Malik on 16/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import MapKit

// MARK:- VIEW DELEGATE PROTOCOL
protocol BarsMapViewModelViewDelegate: class {
    func barAnnotationsLoaded(with result: Result<[MKAnnotation]>)
}

protocol BarsMapViewModelProtocol {
    func searchNearbyBars()
}

class BarsMapViewModel: BarsMapViewModelProtocol {
    // MARK: PRIVATE VARIABLES
    private let placesManager: PlacesManagerProtocol
    private weak var viewDelegate: BarsMapViewModelViewDelegate!
    
    // MARK: INITIALIZER
    init(placesManager: PlacesManagerProtocol, viewDelegate: BarsMapViewModelViewDelegate) {
        self.placesManager = placesManager
        self.viewDelegate = viewDelegate
    }
    
    func searchNearbyBars() {
        self.placesManager.searchNearbyBars { [weak self] (result: Result<[BarProtocol]>) in
            var vmResult: Result<[MKAnnotation]>!
            switch result {
            case .success(let bars):
                let barsAnnotationsVM = bars.map { return BarViewModel(bar: $0) }
                vmResult = .success(barsAnnotationsVM)
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.barAnnotationsLoaded(with: vmResult)
        }
    }
}

//
//  BarListTableViewDataSource.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class BarListTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: BarListViewModelProtocol?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfBars()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarListTableViewCell", for: indexPath)
        if let barCell = cell as? BarListTableViewCellProtocol,
            let barVM = viewModel?.bar(at: indexPath.row) {
            barCell.configCell(with: barVM)
        }
        return cell
    }
}

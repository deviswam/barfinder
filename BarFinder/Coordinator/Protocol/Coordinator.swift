//
//  Coordinator.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

import Foundation

protocol Coordinator {
    func start()
}

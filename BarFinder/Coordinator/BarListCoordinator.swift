//
//  BarListCoordinator.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class BarListCoordinator: Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var tabbarController: UITabBarController
    
    // MARK:- INITIALIZER
    init(tabbarController: UITabBarController) {
        self.tabbarController = tabbarController
    }
    
    func start() {
        if let barListVC = tabbarController.viewControllers?.first as? BarListViewController {
            let placesManager = SharedComponentsDir.placesManager
            let barListTableViewDataSource = BarListTableViewDataSource()
            let barListTableViewDelegate = BarListTableViewDelegate()
            let barListViewModel = BarListViewModel(placesManager: placesManager, viewDelegate: barListVC, coordinatorDelegate: self)
            
            barListVC.viewModel = barListViewModel
            barListVC.barListTableViewDataSource = barListTableViewDataSource
            barListVC.barListTableViewDelegate = barListTableViewDelegate
            barListTableViewDataSource.viewModel = barListViewModel
            barListTableViewDelegate.viewModel = barListViewModel
        }
    }
}

extension BarListCoordinator: BarListViewModelCoordinatorDelegate {
    func didSelect(bar: BarProtocol) {
        URLHandler.openUrl(ofBar: bar)
    }
}

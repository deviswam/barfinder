//
//  AppCoordinator.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    private let BAR_LIST_KEY = "barList"
    private let BARS_MAP_KEY = "barsMap"
    
    private var window: UIWindow
    private var coordinators = [String: Coordinator]()
    
    // MARK:- INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let tabBarController = storyboard.instantiateInitialViewController() as? UITabBarController {
            self.window.rootViewController = tabBarController
            
            self.loadBarList(tabBarController: tabBarController)
            self.loadMap(tabBarController: tabBarController)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func loadBarList(tabBarController: UITabBarController) {
        let barListCoordinator = BarListCoordinator(tabbarController: tabBarController)
        coordinators[BAR_LIST_KEY] = barListCoordinator
        barListCoordinator.start()
    }
    
    private func loadMap(tabBarController: UITabBarController) {
        let mapCoordinator = MapCoordinator(tabbarController: tabBarController)
        coordinators[BARS_MAP_KEY] = mapCoordinator
        mapCoordinator.start()
    }
}

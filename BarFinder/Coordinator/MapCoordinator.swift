//
//  MapCoordinator.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class MapCoordinator: Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var tabbarController: UITabBarController
    
    // MARK:- INITIALIZER
    init(tabbarController: UITabBarController) {
        self.tabbarController = tabbarController
    }
    
    func start() {
        if let barsMapVC = tabbarController.viewControllers?.last as? BarsMapViewController {
            let placesManager = SharedComponentsDir.placesManager
            let barsMapViewDelegate = BarsMapViewDelegate()
            let barsMapViewModel = BarsMapViewModel(placesManager: placesManager, viewDelegate: barsMapVC)
            
            barsMapVC.viewModel = barsMapViewModel
            barsMapVC.barsMapViewDelegate = barsMapViewDelegate
        }
    }
}

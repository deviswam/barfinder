//
//  PlacesManager.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import CoreLocation

enum PlacesManagerError : LocalizedError {
    case noNearbyBarsFoundError
    case userLocationNotFoundError
    case networkError
    
    var localizedDescription: String {
        switch self {
        case .noNearbyBarsFoundError:
            return "Unable to fetch nearby bars data"
        case .userLocationNotFoundError:
            return "Unable to determine user's device location"
        case .networkError:
            return "Network error"
        }
    }
}

protocol PlacesManagerProtocol {
    // gets list of nearby bars
    func searchNearbyBars(completionHandler: @escaping (_ result: Result<[BarProtocol]>) -> Void)
}

class PlacesManager: PlacesManagerProtocol {
    // MARK: PRIVATE VARIABLES
    private let placesAPIClient: PlacesAPIClient
    private let locationService: LocationServiceProtocol
    private var loadedBars: [BarProtocol]?
    
    // MARK: INITIALIZER
    init(placesAPIClient: PlacesAPIClient, locationService: LocationServiceProtocol) {
        self.placesAPIClient = placesAPIClient
        self.locationService = locationService
    }
    
    // MARK: PUBLIC METHODS
    func searchNearbyBars(completionHandler: @escaping (Result<[BarProtocol]>) -> Void) {
        guard self.loadedBars == nil else {
            completionHandler(.success(self.loadedBars!))
            return
        }
        
        self.locationService.getLocation { [weak self] (locResult: Result<(lat: Double, lon: Double)>) in
            switch locResult {
            case .success(let location):
                self?.placesAPIClient.fetchNearbyBars(around: (latitude: location.lat, longitude: location.lon), for: 1500) { (result: Result<[BarProtocol]>) in
                    var mResult: Result<[BarProtocol]>!
                    switch result {
                    case .failure(let error):
                        mResult = .failure(self!.placesManagerError(from: error as! PlacesAPIClientError))
                    case .success(let bars):
                        self?.loadedBars = self?.calculateDistance(for: bars, from: (lat: location.lat, lon: location.lon))
                        mResult = .success(self!.loadedBars!)
                    }
                    completionHandler(mResult)
                }
            case .failure(_):
                completionHandler(.failure(PlacesManagerError.userLocationNotFoundError))
            }
        }
    }
    
    // MARK: PRIVATE METHODS
    private func calculateDistance(for bars:[BarProtocol], from currentLocation: (lat: Double, lon: Double)) -> [BarProtocol] {
        let currentLocation = CLLocation(latitude: currentLocation.lat, longitude: currentLocation.lon)
        
        return bars.map { (bar: BarProtocol) -> BarProtocol in
            var vbar = bar
            if let lat = vbar.latitude, let lon = vbar.longitude {
              vbar.distance = currentLocation.distance(from: CLLocation(latitude: lat, longitude: lon)) / 1609.344
            }
            return vbar
        }
    }
    
    private func placesManagerError(from apiError: PlacesAPIClientError) -> PlacesManagerError {
        switch apiError {
        case .invalidRequest, .dataSerializationError, .invalidDataError:
            return PlacesManagerError.noNearbyBarsFoundError
        case .httpError:
            return PlacesManagerError.networkError
        }
    }
}

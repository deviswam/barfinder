//
//  SharedComponentsDir.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let placesManager: PlacesManagerProtocol = {
        let placesManager = PlacesManager(placesAPIClient: SharedComponentsDir.placesAPIClient, locationService: SharedComponentsDir.locationService)
        return placesManager
    }()
    
    static let placesAPIClient: PlacesAPIClient = {
        let googlePlacesAPIClient = GooglePlacesAPIClient()
        return googlePlacesAPIClient
    }()
    
    static let locationService: LocationServiceProtocol = {
        let locationService = LocationService()
        return locationService
    }()
}

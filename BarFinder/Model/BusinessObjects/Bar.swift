//
//  Bar.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol BarProtocol {
    var id: String { get }
    var name: String? { get set }
    var latitude: Double? { get set }
    var longitude: Double? { get set }
    var distance: Double? { get set } // bar distrance to current location in miles
}

struct Bar: BarProtocol, ObjectMapper {
    var id: String
    var name: String?
    var latitude: Double?
    var longitude: Double?
    var distance: Double?
    
    init(id: String) {
        self.id = id
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
            let dId = dictionary["place_id"] as? String else {
                return nil
        }
        
        self.init(id: dId)
        
        self.name = dictionary["name"] as? String
        
        if let geometryDict = dictionary["geometry"] as? [String: Any],
            let locationDict = geometryDict["location"] as? [String: Any],
            let latitude =  locationDict["lat"] as? Double,
            let longitude = locationDict["lng"] as? Double {
                self.latitude = latitude
                self.longitude = longitude
        }
    }
}

func == (lhs: BarProtocol, rhs: BarProtocol) -> Bool {
    return lhs.id == rhs.id
}

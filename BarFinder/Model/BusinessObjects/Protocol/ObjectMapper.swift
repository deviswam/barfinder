//
//  ObjectMapper.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol ObjectMapper {
    init?(dictionary:Dictionary<String,Any>?)
}

//
//  LocationService.swift
//  BarFinder
//
//  Created by Waheed Malik on 16/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import CoreLocation
import UIKit

enum LocationServiceError: Error {
    case unableToLocate
    case accessDenied
    case unknown
}

protocol LocationServiceProtocol {
    func getLocation(completionHandler: @escaping (_ result: Result<(lat: Double, lon: Double)>) -> Void)
}

class LocationService: NSObject, LocationServiceProtocol {
    fileprivate var completionHandler: ((_ result: Result<(lat: Double, lon: Double)>) -> Void)?
    
    fileprivate var currentLocation: CLLocation? {
        didSet {
            if let currentLocation = currentLocation {
                print("WAM: Location received: \(currentLocation)")
                self.completionHandler?(.success((currentLocation.coordinate.latitude,  currentLocation.coordinate.longitude)))
            }
        }
    }
    
    private lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        return locationManager
    }()
    
    func getLocation(completionHandler: @escaping (_ result: Result<(lat: Double, lon: Double)>) -> Void) {
        guard self.currentLocation == nil else {
            completionHandler(.success((currentLocation!.coordinate.latitude,  currentLocation!.coordinate.longitude)))
            return
        }
        
        self.completionHandler = completionHandler
        
        let status  = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        if status == .denied || status == .restricted {
            print("Please enable Location Services in Settings")
            return
        }
        
        locationManager.requestLocation()
    }
}

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        } else if status == .denied {
            print("WAM: You denied location service")
        } else if status == .notDetermined {
            manager.requestWhenInUseAuthorization()
            print("WAM: location service undetermined autherization")
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            let serviceError = self.locationServiceError(from: error)
            self.completionHandler?(.failure(serviceError))
    }
    
    private func locationServiceError(from error: Error) -> LocationServiceError {
        switch error {
        case CLError.locationUnknown:
            return .unableToLocate
        case CLError.denied:
            return .accessDenied
        default:
            return .unknown
        }
    }
}

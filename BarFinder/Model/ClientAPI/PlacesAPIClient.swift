//
//  PlacesAPIClient.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum PlacesAPIClientError: Error {
    case invalidRequest
    case httpError
    case invalidDataError
    case dataSerializationError
}

enum GooglePlacesAPIClientMethod: String {
    case nearbysearch = "nearbysearch"
}

protocol PlacesAPIClient {
    // gets list of nearby bars
    func fetchNearbyBars(around location:(latitude: Double, longitude: Double), for radius: Int, completionHandler: @escaping (_ result: Result<[BarProtocol]>) -> Void)
}

class GooglePlacesAPIClient: PlacesAPIClient {
    // MARK: PRIVATE VARIABLES
    private let BASE_URL = "https://maps.googleapis.com/maps/api/place/"
    private let API_Key = "AIzaSyAhhCLxQoIGUWrQBVmbMY2zWlqYlI_GLCE"
    
    private let urlSession: URLSession!
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // gets list of nearby bars
    func fetchNearbyBars(around location:(latitude: Double, longitude: Double), for radius: Int, completionHandler: @escaping (_ result: Result<[BarProtocol]>) -> Void) {
        let urlStr = BASE_URL + GooglePlacesAPIClientMethod.nearbysearch.rawValue + "/json"
        let requestParams = ["location": "\(location.latitude),\(location.longitude)",
                             "radius": "\(radius)",
                             "type": "bar",
                             "key": API_Key]
        guard let urlRequest = self.createURLRequest(urlString: urlStr, parameters: requestParams) else {
            completionHandler(.failure(PlacesAPIClientError.invalidRequest))
            return
        }
        self.getNearbyBars(with: urlRequest) { (result: Result<[BarProtocol]>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func getNearbyBars(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<[BarProtocol]>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { [unowned self] (data, urlResponse, error) in
            var result: Result<[BarProtocol]>!
            
            if error != nil {
                result = .failure(PlacesAPIClientError.httpError)
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let barResultsArray = dictionary["results"] as? [[String : AnyObject]] else {
                                DispatchQueue.main.async {
                                    completionHandler(.failure(PlacesAPIClientError.invalidDataError))
                                }
                                return
                        }
                        result = .success(self.createBars(from: barResultsArray))
                    }
                } catch {
                    result = .failure(PlacesAPIClientError.dataSerializationError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func createBars(from barsArray:[[String : AnyObject]]) -> [BarProtocol] {
        let bars = barsArray.map { (barDictionary: [String : AnyObject]) -> BarProtocol? in
            return Bar(dictionary: barDictionary)
            }.filter {
                return $0 != nil
            } as! [BarProtocol]
        
        return bars
    }

    
    private func createURLRequest(urlString: String, parameters:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: urlString)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        print("WAM URL:\(url.absoluteString)")
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
}

//
//  PlacesManagerTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

extension PlacesManagerTests {
    class MockPlacesAPIClient: PlacesAPIClient {
        var fetchNearbyBarsCalled = false
        var completionHandler: ((_ result: Result<[BarProtocol]>) -> Void)?
        func fetchNearbyBars(around location:(latitude: Double, longitude: Double), for radius: Int, completionHandler: @escaping (_ result: Result<[BarProtocol]>) -> Void) {
            fetchNearbyBarsCalled = true
            self.completionHandler = completionHandler
        }
    }
    
    class StubLocationService: LocationServiceProtocol {
        func getLocation(completionHandler: @escaping (Result<(lat: Double, lon: Double)>) -> Void) {
            completionHandler(.success((51.491455, 0.070726)))
        }
    }
}

class PlacesManagerTests: XCTestCase {
    var mockPlacesAPIClient: MockPlacesAPIClient!
    var stubLocationService: StubLocationService!
    var sut: PlacesManager!
    
    override func setUp() {
        super.setUp()
        mockPlacesAPIClient = MockPlacesAPIClient()
        stubLocationService = StubLocationService()
        sut = PlacesManager(placesAPIClient: mockPlacesAPIClient, locationService: stubLocationService)
    }
    
    func testConformanceToPlacesManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is PlacesManagerProtocol, "PlacesManager should conforms to PlacesManagerProtocol")
    }
    
    func testSearchNearbyBars_asksPlacesAPIClientToFetchNearbyBars() {
        // Act
        sut.searchNearbyBars { (result: Result<[BarProtocol]>) in
            
        }
        
        // Assert
        XCTAssert(mockPlacesAPIClient.fetchNearbyBarsCalled)
    }
    
    func testSearchNearbyBars_shouldReceiveBarsCollectionWithNoError_whenBarsFound() {
        // Arrange
        var receivedBars : [BarProtocol]?
        var receivedError : Error?
        let expectedBars = [Bar(id: "abcd")]
        
        // Act
        sut.searchNearbyBars { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error
            }
        }
        mockPlacesAPIClient.completionHandler?(.success(expectedBars))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedBars)
        XCTAssert(expectedBars.first! == receivedBars!.first!)
    }
    
    func testSearchNearbyBars_shouldReceiveNoNearbyBarsFoundError_whenAPIClientHasInvalidDataError() {
        // Arrange
        var receivedBars : [BarProtocol]?
        var receivedError : Error?
        
        // Act
        sut.searchNearbyBars { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error
            }
        }
        mockPlacesAPIClient.completionHandler?(.failure(PlacesAPIClientError.invalidDataError))
        
        //Assert
        XCTAssertEqual(receivedError as! PlacesManagerError, PlacesManagerError.noNearbyBarsFoundError, "Should receive NoNearbyBarsFoundError when underlying api has InvalidDataError")
        XCTAssertNil(receivedBars, "No bars are returned when error occured")
    }
    
    func testSearchNearbyBars_shouldReceiveNoNearbyBarsFoundError_whenAPIClientHasDataSerializationError() {
        // Arrange
        var receivedBars : [BarProtocol]?
        var receivedError : Error?
        
        // Act
        sut.searchNearbyBars { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error
            }
        }
        mockPlacesAPIClient.completionHandler?(.failure(PlacesAPIClientError.dataSerializationError))
        
        //Assert
        XCTAssertEqual(receivedError as! PlacesManagerError, PlacesManagerError.noNearbyBarsFoundError, "Should receive NoNearbyBarsFoundError when underlying api has data serialization error")
        XCTAssertNil(receivedBars, "No bars are returned when error occured")
    }
    
    func testSearchNearbyBars_shouldReceiveNetworkError_whenAPIClientHasHttpError() {
        // Arrange
        var receivedBars : [BarProtocol]?
        var receivedError : Error?
        
        // Act
        sut.searchNearbyBars { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error
            }
        }
        mockPlacesAPIClient.completionHandler?(.failure(PlacesAPIClientError.httpError))
        
        //Assert
        XCTAssertEqual(receivedError as! PlacesManagerError, PlacesManagerError.networkError, "Should receive Network error when underlying api has http error")
        XCTAssertNil(receivedBars, "No bars are returned when error occured")
    }
}

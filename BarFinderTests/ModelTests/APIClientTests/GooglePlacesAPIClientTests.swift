//
//  GooglePlacesAPIClientTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

extension GooglePlacesAPIClientTests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class GooglePlacesAPIClientTests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: GooglePlacesAPIClient!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = GooglePlacesAPIClient(urlSession: mockURLSession)
    }
    
    func testConformanceToPlacesAPIClientProtocol() {
        XCTAssertTrue((sut as AnyObject) is PlacesAPIClient)
    }
    
    func testFetchNearbyBars_ShouldAskURLSessionForFetchingNearbyBarsData() {
        // Act
        sut.fetchNearbyBars(around: (latitude: 12.000, longitude: 0.34), for: 500) { (result: Result<[BarProtocol]>) in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testFetchNearbyBars_WithValidJSONOfABar_ShouldReturnABarObject() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let googlePlacesAPIResponseInString = "{\"results\":[{" +
            "\"name\": \"Favourite Inn\"," +
            "\"geometry\":{" +
            "\"location\":{" +
            "\"lat\": 51.4902944," +
            "\"lng\": 0.0700711" +
            "}" +
            "}," +
            "\"place_id\": \"ChIJt6YYdOqo2EcR8M3uTls2x50\"" +
        "}]}"
        
        let responseData = googlePlacesAPIResponseInString.data(using: .utf8)
        let expectedBarId = "ChIJt6YYdOqo2EcR8M3uTls2x50"
        var receivedBar : BarProtocol?
        var receivedError : PlacesAPIClientError?
        
        //Act
        sut.fetchNearbyBars(around: (latitude: 12.0, longitude: 0.067), for: 500) { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBar = bars.first
            case .failure(let error):
                receivedError = error as? PlacesAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with bar object")
            XCTAssertTrue(receivedBar?.id == expectedBarId, "Fetched and expected Bar should have same id")
            XCTAssertTrue(receivedBar?.name == "Favourite Inn", "Fetched and expected Bar should have same name")
            XCTAssertTrue(receivedBar?.latitude == 51.4902944, "Fetched and expected Bar should have same latitude")
            XCTAssertTrue(receivedBar?.longitude == 0.0700711, "Fetched and expected Bar should have same longitude")
        }
    }
    
    func testFetchNearbyBars_WithValidJSONOfTwoBars_ShouldReturnTwoBarObject() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let googlePlacesAPIResponseInString = "{\"results\":[{" +
            "\"name\": \"Favourite Inn\"," +
            "\"geometry\":{" +
            "\"location\":{" +
            "\"lat\": 51.4902944," +
            "\"lng\": 0.0700711" +
            "}" +
            "}," +
            "\"place_id\": \"ChIJt6YYdOqo2EcR8M3uTls2x50\"" +
            "},{" +
            "\"name\": \"Dial Arch\"," +
            "\"geometry\":{" +
            "\"location\":{" +
            "\"lat\": 51.4925635," +
            "\"lng\": 0.0700711" +
            "}" +
            "}," +
            "\"place_id\": \"ChIJESyFk-qo2EcRbNMfeQk6gmQ\"" +
        "}]}"
        
        let responseData = googlePlacesAPIResponseInString.data(using: .utf8)
        var receivedBars : [BarProtocol]?
        var receivedError : PlacesAPIClientError?
        
        //Act
        sut.fetchNearbyBars(around: (latitude: 12.0, longitude: 0.067), for: 500) { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error as? PlacesAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with bar object")
            
            XCTAssertTrue(receivedBars?.first?.id == "ChIJt6YYdOqo2EcR8M3uTls2x50", "Fetched and expected Bar should have same id")
            XCTAssertTrue(receivedBars?.first?.name == "Favourite Inn", "Fetched and expected Bar should have same name")
            XCTAssertTrue(receivedBars?.first?.latitude == 51.4902944, "Fetched and expected Bar should have same latitude")
            XCTAssertTrue(receivedBars?.first?.longitude == 0.0700711, "Fetched and expected Bar should have same longitude")
            
            XCTAssertTrue(receivedBars?.last?.id == "ChIJESyFk-qo2EcRbNMfeQk6gmQ", "Fetched and expected Bar should have same id")
            XCTAssertTrue(receivedBars?.last?.name == "Dial Arch", "Fetched and expected Bar should have same name")
            XCTAssertTrue(receivedBars?.last?.latitude == 51.4925635, "Fetched and expected Bar should have same latitude")
            XCTAssertTrue(receivedBars?.last?.longitude == 0.0700711, "Fetched and expected Bar should have same longitude")
        }
    }
    
    func testFetchNearbyBars_withInValidJSONOfABar_shouldReturnAnInvalidJSONErrorAndNilBars() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let googlePlacesAPIResponseInString = "{\"result\":[{" +
            "\"name\": \"Favourite Inn\"," +
            "\"geometry\":{" +
            "\"location\":{" +
            "\"lat\": 51.4902944," +
            "\"lng\": 0.0700711" +
            "}" +
            "}," +
            "\"place_id\": \"ChIJt6YYdOqo2EcR8M3uTls2x50\"" +
        "}]}"
        
        let responseData = googlePlacesAPIResponseInString.data(using: .utf8)
        var receivedBars : [BarProtocol]?
        var receivedError : PlacesAPIClientError?
        
        //Act
        sut.fetchNearbyBars(around: (latitude: 12.0, longitude: 0.067), for: 500) { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error as? PlacesAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError! == PlacesAPIClientError.invalidDataError, "Invalid bar JSON should return an invalidData Error")
            XCTAssertNil(receivedBars, "Invalid Result JSON should return bars array as Nil")
        }
    }
    
    func testFetchNearbyBars_withEmptyData_shouldReturnSerializerErrorAndNilBars() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        var receivedBars : [BarProtocol]?
        var receivedError : PlacesAPIClientError?
        
        //Act
        sut.fetchNearbyBars(around: (latitude: 12.0, longitude: 0.067), for: 500) { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error as? PlacesAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(Data(), nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError! == PlacesAPIClientError.dataSerializationError, "Empty Data() should return serializer Error")
            XCTAssertNil(receivedBars, "Invalid Bars JSON should return bars array as Nil")
        }
    }
    
    func testFetchNearbyBars_withHttpError_shouldReturnHttpErrorAndNilBars() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let googlePlacesAPIResponseInString = "{\"results\":[{" +
            "\"name\": \"Favourite Inn\"," +
            "\"geometry\":{" +
            "\"location\":{" +
            "\"lat\": 51.4902944," +
            "\"lng\": 0.0700711" +
            "}" +
            "}," +
            "\"place_id\": \"ChIJt6YYdOqo2EcR8M3uTls2x50\"" +
        "}]}"
        
        let responseData = googlePlacesAPIResponseInString.data(using: .utf8)
        var receivedBars : [BarProtocol]?
        var receivedError : PlacesAPIClientError?
        let expectedError = PlacesAPIClientError.httpError
        
        //Act
        sut.fetchNearbyBars(around: (latitude: 12.0, longitude: 0.067), for: 500) { (result: Result<[BarProtocol]>) in
            switch result {
            case .success(let bars):
                receivedBars = bars
            case .failure(let error):
                receivedError = error as? PlacesAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, expectedError)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError == expectedError, "Session HTTP Error should return a http error")
            XCTAssertNil(receivedBars, "Bars array should be Nil when httpError occurs")
        }
    }
    
    func testFetchNearbyBars_shouldCallResumeMethodOfDataTaskToFireupRequest() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        //Act
        sut.fetchNearbyBars(around: (latitude: 12.0, longitude: 0.067), for: 500) { (result: Result<[BarProtocol]>) in
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(Data(), nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(self.mockURLSession.dataTask.resumeGotCalled, "Fetch Nearby bars method should asks Resume method of Data Task to trigger request")
        }
    }

}

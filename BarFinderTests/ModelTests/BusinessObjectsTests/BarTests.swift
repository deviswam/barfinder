//
//  BarTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

class BarTests: XCTestCase {
    
    func testConformanceToBarProtocol() {
        // Arrange
        let sut = Bar(id: "ChIJt6YYdOqo2EcR8M3uTls2x50")
        // Assert
        XCTAssert((sut as Any) is BarProtocol)
    }
    
    func testInit_barMustBeCreatedWithABarId() {
        // Arrange
        let barId = "ChIJt6YYdOqo2EcR8M3uTls2x50"
        let sut = Bar(id: barId)
        // Assert
        XCTAssertEqual(sut.id, barId)
    }
    
    func testInit_barShouldHaveAName() {
        // Arrange
        let barName = "Famous bar"
        var sut = Bar(id: "ChIJt6YYdOqo2EcR8M3uTls2x50")
        sut.name = barName
        // Assert
        XCTAssertEqual(sut.name, barName)
    }
    
    func testInit_barShouldHaveALatitude() {
        // Arrange
        let barLatitude = 51.4902944
        var sut = Bar(id: "ChIJt6YYdOqo2EcR8M3uTls2x50")
        sut.latitude = barLatitude
        // Assert
        XCTAssertEqual(sut.latitude, barLatitude)
    }
    
    func testInit_barShouldHaveALongitude() {
        // Arrange
        let barLongitude = 51.4902944
        var sut = Bar(id: "ChIJt6YYdOqo2EcR8M3uTls2x50")
        sut.longitude = barLongitude
        // Assert
        XCTAssertEqual(sut.longitude, barLongitude)
    }
    
    func testBarConformsToObjectMapperProtocol() {
        //Arrange
        let sut = Bar(id: "ChIJt6YYdOqo2EcR8M3uTls2x50")
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "Bar should conforms to ObjectMapper protocol")
    }
    
    func testInit_withNilDictionary_shouldFailInitialization() {
        //Arrange
        let sut = Bar(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "Bar should be created only with valid json dictionary")
    }
    
    func testInit_withNoPlaceIdInDictionary_shouldFailInitialization() {
        //Arrange
        let barDictionary = [String: Any]()
        let sut = Bar(dictionary: barDictionary)
        
        //Assert
        XCTAssertNil(sut, "Bar should only be created if dictionary has id")
    }
    
    func testInit_withNameInDictionary_shouldSetBarName() {
        //Arrange
        let barDictionary = ["place_id": "ChIJt6YYdOqo2EcR8M3uTls2x50", "name": "Famous bar"]
        let sut = Bar(dictionary: barDictionary)
        
        //Assert
        XCTAssertEqual(sut?.name, "Famous bar")
    }
    
    func testInit_withLatitudeAndLongitudeInDictionary_shouldSetLatitudeAndLongitude() {
        //Arrange
        let locationDict = ["lat": 51.4902944, "lng": 0.0700711]
        let geometryDict = ["location": locationDict]
        let barDictionary = ["place_id": "ChIJt6YYdOqo2EcR8M3uTls2x50", "geometry": geometryDict] as [String : Any]
        let sut = Bar(dictionary: barDictionary)
        
        //Assert
        XCTAssertEqual(sut?.latitude, 51.4902944)
        XCTAssertEqual(sut?.longitude, 0.0700711)
    }
}

//
//  BarListTableViewCellTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

extension BarListTableViewCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return BarListTableViewCell()
        }
    }
    
    class FakeBarViewModel: BarViewModelProtocol {
        var name: String { return "Famous Bar" }
        var distance: String { return "0.07m"}
    }
}

class BarListTableViewCellTests: XCTestCase {
    
    let fakeDataSource = FakeDataSource()
    var tableView : UITableView!
    var cell : BarListTableViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let barlistVC = storyBoard.instantiateViewController(withIdentifier: "BarListViewController") as! BarListViewController
        barlistVC.barListTableViewDataSource = fakeDataSource
        UIApplication.shared.keyWindow?.rootViewController = barlistVC
        _ = barlistVC.view
        tableView = barlistVC.barListTableView
        cell = tableView.dequeueReusableCell(withIdentifier: "BarListTableViewCell", for: IndexPath(row: 0, section: 0)) as! BarListTableViewCell
    }
    
    func testConfigCell_SetBarNameOnTextLabel() {
        //Act
        cell.configCell(with: FakeBarViewModel())
        
        //Assert
        XCTAssertEqual(cell.textLabel?.text!, "Famous Bar")
    }
    
    func testConfigCell_SetBarDistanceOnDetailTextLabel() {
        //Act
        cell.configCell(with: FakeBarViewModel())
        
        //Assert
        XCTAssertEqual(cell.detailTextLabel?.text!, "0.07m")
    }
}

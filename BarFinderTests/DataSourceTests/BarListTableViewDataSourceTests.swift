//
//  BarListTableViewDataSourceTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

extension BarListTableViewDataSourceTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockBarListTableViewCell: UITableViewCell, BarListTableViewCellProtocol {
        var configCellGotCalled = false
        var bar : BarViewModelProtocol?
        func configCell(with barViewModel: BarViewModelProtocol) {
            configCellGotCalled = true
            self.bar = barViewModel
        }
    }
    
    class MockBarListViewModel: BarListViewModelProtocol {
        var bars: [BarProtocol]?
        func searchNearbyBars() {
            
        }
        
        func numberOfBars() -> Int {
            return bars!.count
        }
        
        func bar(at index: Int) -> BarViewModelProtocol? {
            return BarViewModel(bar: bars![index])
        }
        
        func selectedBar(at index: Int) {
            
        }
    }
}

class BarListTableViewDataSourceTests: XCTestCase {
    func testDataSourceHasBarListViewModel() {
        // Arrange
        let sut = BarListTableViewDataSource()
        sut.viewModel = MockBarListViewModel()
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is BarListViewModelProtocol)
    }
    
    func testNumberOfRowsInTheSection_WithTwoBars_ShouldReturnTwo() {
        //Arrange
        let sut = BarListTableViewDataSource()
        let bars = [Bar(id: "123"), Bar(id: "456")]
        let mockBarListViewModel = MockBarListViewModel()
        mockBarListViewModel.bars = bars
        
        sut.viewModel = mockBarListViewModel
        let mockTableView = MockTableView()
        
        //Act
        let noOfItemsInSectionZero = sut.tableView(mockTableView, numberOfRowsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section 0 are 2")
    }
    
    func testCellForRow_ReturnsBarListTableViewCell() {
        //Arrange
        let sut = BarListTableViewDataSource()
        let bars = [Bar(id: "123"), Bar(id: "456")]
        let mockBarListViewModel = MockBarListViewModel()
        mockBarListViewModel.bars = bars
        
        sut.viewModel = mockBarListViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockBarListTableViewCell.self, forCellReuseIdentifier: "BarListTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is BarListTableViewCellProtocol,"Returned cell is of BarListTableViewCellProtocol type")
    }
    
    func testCellForRow_DequeuesCell() {
        //Arrange
        let sut = BarListTableViewDataSource()
        let bars = [Bar(id: "123"), Bar(id: "456")]
        let mockBarListViewModel = MockBarListViewModel()
        mockBarListViewModel.bars = bars
        
        sut.viewModel = mockBarListViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockBarListTableViewCell.self, forCellReuseIdentifier: "BarListTableViewCell")
        
        //Act
        _ = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockTableView.cellGotDequeued,"CellForRow should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForRow() {
        //Arrange
        let sut = BarListTableViewDataSource()
        var bar1 = Bar(id: "123")
        bar1.name = "Famous Bar"
        let bars = [bar1, Bar(id: "456")]
        let mockBarListViewModel = MockBarListViewModel()
        mockBarListViewModel.bars = bars
        
        sut.viewModel = mockBarListViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockBarListTableViewCell.self, forCellReuseIdentifier: "BarListTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0)) as! MockBarListTableViewCell
        
        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForRow should be calling ConfigCell method")
        XCTAssertTrue(cell.bar?.name == bars.first?.name, "Bar name should be same")
    }
}

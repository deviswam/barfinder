//
//  BarListViewControllerTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 15/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

extension BarListViewControllerTests {
    class MockBarListViewModel: BarListViewModelProtocol {
        var bars: [BarProtocol]?
        var searchNearbyBarsCalled = false
        func searchNearbyBars() {
            searchNearbyBarsCalled = true
        }
        
        func numberOfBars() -> Int {
            return bars!.count
        }
        
        func bar(at index: Int) -> BarViewModelProtocol? {
            return BarViewModel(bar: bars![index])
        }
        
        func selectedBar(at index: Int) {
            
        }
    }
    
    class MockUITableView: UITableView {
        var reloadGotCalled = false
        override func reloadData() {
            reloadGotCalled = true
        }
    }
}

class BarListViewControllerTests: XCTestCase {
    var sut: BarListViewController!
    var mockViewModel: MockBarListViewModel!
    
    override func setUp() {
        super.setUp()
        
        // Arrange
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "BarListViewController") as! BarListViewController
        mockViewModel = MockBarListViewModel()
        sut.viewModel = mockViewModel
    }
    
    
    func testBarListTableView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.barListTableView)
    }
    
    
    func testBarListTableView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.barListTableViewDataSource = BarListTableViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.barListTableView.dataSource is BarListTableViewDataSource)
    }
    
    func testBarListTableView_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.barListTableViewDelegate = BarListTableViewDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.barListTableView.delegate is BarListTableViewDelegate)
    }
    
    func testViewLoad_shouldAskViewModelForNearbyBars() {
        //Act
        _ = sut.view
        
        //Assert
        XCTAssertTrue(mockViewModel.searchNearbyBarsCalled,"view load should call view model for bars")
    }
    
    func testAfterHavingBars_ShouldReloadTableView() {
        //Arrange
        let mockUITableView = MockUITableView(frame: CGRect.zero)
        sut.barListTableView = mockUITableView
        
        //Act
        sut.barsLoaded(with: .success())
        
        //Assert
        XCTAssertTrue(mockUITableView.reloadGotCalled,"TableView Reload method should be called")
    }

}

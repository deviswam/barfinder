//
//  BarListViewModelTests.swift
//  BarFinder
//
//  Created by Waheed Malik on 14/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BarFinder

extension BarListViewModelTests {
    class MockPlacesManager: PlacesManagerProtocol {
        var searchNearbyBars = false
        var completionHandler: ((_ result: Result<[BarProtocol]>) -> Void)?
        func searchNearbyBars(completionHandler: @escaping (_ result: Result<[BarProtocol]>) -> Void) {
            searchNearbyBars = true
            self.completionHandler = completionHandler
        }
    }
    
    class MockBarListViewModelViewDelegate: BarListViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var barListViewModelResult: Result<Void>?
        
        func barsLoaded(with result: Result<Void>) {
            guard let asynExpectation = expectation else {
                XCTFail("MockBarListViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.barListViewModelResult = result
            asynExpectation.fulfill()
        }
    }
    
    class MockBarListViewModelCoordinatorDelegate: BarListViewModelCoordinatorDelegate {
        func didSelect(bar: BarProtocol) {
            
        }
    }
}

class BarListViewModelTests: XCTestCase {
    
    var sut: BarListViewModel!
    var mockPlacesManager: MockPlacesManager!
    var mockViewDelegate: MockBarListViewModelViewDelegate!
    var mockCoordinatorDelegate: MockBarListViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        mockPlacesManager = MockPlacesManager()
        mockViewDelegate = MockBarListViewModelViewDelegate()
        mockCoordinatorDelegate = MockBarListViewModelCoordinatorDelegate()
        sut = BarListViewModel(placesManager: mockPlacesManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testConformanceToBarListViewModelProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is BarListViewModelProtocol, "BarListViewModel should conforms to BarListViewModelProtocol")
    }
    
    func testSearchNearbyBars_shouldAskPlacesManagerForCollectionOfNearByBars() {
        // Act
        sut.searchNearbyBars()
        // Assert
        XCTAssert(mockPlacesManager.searchNearbyBars)
    }
    
    func testSearchNearbyBars_whenBarsFound_raisesBarsLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.success([Bar(id: "123")]))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.barListViewModelResult! {
            case .success():
                XCTAssert(true)
            default:
                XCTFail("View delegate should only receive success result when bars found")
            }
        }
    }
    
    func testSearchNearbyBars_whenNoBarsFound_raisesBarsLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.failure(PlacesManagerError.noNearbyBarsFoundError))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.barListViewModelResult! {
            case .success():
                XCTFail("View delegate should receive failure with an error when NO bars found")
            case .failure(let error):
                XCTAssertEqual(error as! PlacesManagerError, PlacesManagerError.noNearbyBarsFoundError)
            }
        }
    }
    
    func testNumberOfBars_whenNoBarsFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.failure(PlacesManagerError.noNearbyBarsFoundError))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfBars(), 0)
        }
    }
    
    func testNumberOfBars_whenOneBarFound_returnsOne() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.success([Bar(id: "1")]))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfBars(), 1)
        }
    }
    
    func testNumberOfBars_whenFourBarsFound_returnsFour() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.success([Bar(id: "1"), Bar(id: "2"), Bar(id: "3"), Bar(id: "4")]))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfBars(), 4)
        }
    }
    
    func testBarAtIndex_returnsTheCorrectBarViewModelObject() {
        // Arrange
        var bar1 = Bar(id: "1")
        bar1.name = "Famous Bar 1"
        var bar2 = Bar(id: "2")
        bar2.name = "Famous Bar 2"
        
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.success([bar1, bar2]))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let bar1 = self.sut.bar(at: 0)
            let bar2 = self.sut.bar(at: 1)
            XCTAssertEqual(bar1?.name, "Famous Bar 1")
            XCTAssertEqual(bar2?.name, "Famous Bar 2")
        }
    }
    
    func testBarAtIndex_whenBarNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        var bar1 = Bar(id: "1")
        bar1.name = "Famous Bar 1"
        var bar2 = Bar(id: "2")
        bar2.name = "Famous Bar 2"
        
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchNearbyBars()
        mockPlacesManager.completionHandler?(.success([bar1, bar2]))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let bar = self.sut.bar(at: 2)
            XCTAssertNil(bar)
        }
    }
    
    
}
